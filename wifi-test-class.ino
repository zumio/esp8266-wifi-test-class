#include <SoftwareSerial.h>
#include "WifiCore.h"

// Details of your WIFI network
#define SSID  "<your-wifi-ssid>"
#define PSK   "<your-wifi-password>"

// Thingspeak API details
#define API_KEY "<your-api-key>"

using namespace Lormo::Api; // the namespace of the Wifi classes I've been developing

void setup() {
  Serial.begin(9600);

  // setup the wifi connection
  // this sometimes results in errors switching to the correct SSID due to mal-formed
  // responses being returned by the ESP8266 AT commands
  // but... generally it works OK.  It's not as robust as I'd like (i.e. retries and
  // re-establishing connections when dropped
  WifiConnection wifi = WifiConnection();
  wifi.begin();
  //wifi.hardReset(); // not yet working/implemented (the code is there, but it didn't seem to work in my tests)
  //wifi.disconnect(); // uncomment to test connecting to a network fresh, rather than being already connected
  wifi.connect(SSID, PSK);
}


long prevMillis = millis();
int interval = 15000; // allow 15 secs between requests

void loop() {
  // every 15 seconds, attempt sending an HTTP request
  if (millis() - prevMillis >= interval) {
    prevMillis = millis();
    wifiTestHTTPRequest();
  }
}


// Some debug code to pass AT commands/view responses directly while debugging
//  SoftwareSerial espSerial = SoftwareSerial(8,9);
//  void serialEvent() {
//    while(Serial.available()) espSerial.write(Serial.read());
//    while(espSerial.available()) Serial.write(espSerial.read());
//  }





// Sets up a WifiHTTPRequest instance pre-populated with the details for posting to the Thingspeak API
// note: needed "throw()" when this method was returning an object (now implemented passing by reference)
// see http://forum.arduino.cc/index.php?topic=243464.0 which points to
//     http://arduino.land/FAQ/content/2/13/en/my-class_reference-won_t-work-in-the-sketch.html
bool _thingSpeakHTTPRequest(WifiHTTPRequest &request) throw() {
  //  WifiHTTPRequest request = WifiHTTPRequest();
  // can leave port (4th param) blank as we're using default port
  request.begin("api.thingspeak.com", "/update", "POST");
  // could also do this by setting params individually
//  request.setHostName = "api.thingspeak.com";
//  request.setHostPort = 80; // already 80 by default
//  request.setEndPoint = "/update";
  request.addHeaderLine("X-THINGSPEAKAPIKEY", API_KEY);
  return true;
}


// Constructs then sends + receives response for an HTTP request (POST method), sending data
// to the Thingspeak API.
int wifiTestHTTPRequest() {
  // originally had _thingSpeakHTTPRequest() return the request instance/object
  // but updated to pass by reference in case this was causing memory issues
  WifiHTTPRequest request = WifiHTTPRequest();
  _thingSpeakHTTPRequest(request);
  
  // setup some dummy data (to come from sensors/EEPROM etc.)
  char siteID[] = "001";
  char monitorID[] = "001";
  char sensorType[] = "elec";
  float f_curr = 0.23f;
  float f_volt = 240.0f;
  // snprintf and other methods don't support floats in Arduino, so convert to a C string
  char c_curr[6];
  dtostrf(f_curr, 4, 2, c_curr);
  char c_volt[7];
  dtostrf(f_volt, 4, 2, c_volt);

  // We're using the Thingspeak API POST method
  // Construct the request body, expected in the POST method format of fieldname=value&fieldname=value
  // You could use a similar approach for GET method requests
  char params[64]; // 60 is expected max length
  snprintf(params, sizeof(params), "field1=%s&field2=%s&field3=%s&field4=%s&field5=%s", siteID, monitorID, sensorType, c_curr, c_volt);
  request.setBody(String(params));
  // TODO: create overload for setBody() that supports char array being passed
  
  // rendering the request and outputting to the Serial does not cause any issues
//  char reqString[256]{0};
//  request.render(reqString, 256);
//  Serial.println(reqString);

  // attempting to use the same when sending using an AT command causes issues
  request.send();
  // e.g. after a few attempts the ESP8266 stops responding or Serial.println statements 
  // stop appearing in the Serial monitor (suggesting an out of memory error)
  
  // the response class is not yet fully implemented/tested, but the basic gist would be
//  WifiHTTPResponse response = WifiHTTPResponse();
//  response.begin();
//  response.handleResponse();
//  if (response.isReady()) {
//    if (response.status != 200) {
//      Serial.print("HTTP error ");
//      Serial.println(response.status);
//    }
//  }
}

