#include "Arduino.h"
#include <SoftwareSerial.h>

#define RX_PIN (8) // receive pin for SoftwareSerial
#define TX_PIN (9) // transmit pin for SoftwareSerial
#define RST_PIN (12) // pin for sending hardware reset (LOW) message to ESP8266
#define ESP_BAUD (9600)
#define AT_TIMEOUT (5000) // 2 secs default timeout for AT commands
#define HTTP_TIMEOUT (5000) // 5 secs default timeout for HTTP-related commands

namespace Lormo {
  namespace Api {
    class WifiBase {
      public:
        virtual ~WifiBase() {} // marks as abstract
        WifiBase();
        void begin();
        void begin(SoftwareSerial& serial);
        void sendAT(char cmd[32]);
        void sendAT(String cmd);
        void hardReset();
      protected:
        SoftwareSerial _espSerial = SoftwareSerial(RX_PIN, TX_PIN);
        bool containsString(String searchFor, String searchIn);
        void emptySerialBuffer();
        void printResponse();
        String getATResponseUpToString(String target, uint32_t timeout);
        String getATResponse(uint32_t timeout);
        bool ATResponseContainsString(String target, uint32_t timeout);
        bool getATResponseSubstring(String begin, String end, String &rtn, uint32_t timeout);
        String getSubstring(const String &content, const String &begin, const String &end);
    };


  
    class WifiConnection: public WifiBase {
      public:
        WifiConnection();
        void begin();
        void begin(SoftwareSerial& serial);
        bool connect(char ssid[64], char psk[64]);
        bool disconnect();
      private:
        char _ssid[64];
        char _psk[64];
    };


  
    class WifiHTTPResponse: public WifiBase {
      public:
        WifiHTTPResponse();
        void begin();
        void begin(SoftwareSerial& serial);
        bool isReady();
        int handleResponse();
      protected:
        uint32_t recv(uint8_t *buffer, uint32_t buffer_size, uint32_t timeout);
        uint32_t recv(uint8_t mux_id, uint8_t *buffer, uint32_t buffer_size, uint32_t timeout);
        uint32_t recv(uint8_t *coming_mux_id, uint8_t *buffer, uint32_t buffer_size, uint32_t timeout);
        uint32_t recvPkg(uint8_t *buffer, uint32_t buffer_size, uint32_t *data_len, uint32_t timeout, uint8_t *coming_mux_id);
    };


    
    class WifiHTTPRequest: public WifiBase {
      public:
        WifiHTTPRequest();
        void begin();
        void begin(char hname[64], char path[64], char meth[9] = "GET", int hport=80);
        bool isReady();
        void addHeaderLine(String headerName, String headerValue);
        int send();
        bool setHostName(char hname[64]);
        bool setHostPort(int hport);
        bool setEndPoint(char path[64]);
        bool setMethod(char meth[9]);
        bool setBody(String body);
        bool render(char *buffer, int bufferSize); // mark as public as you often need this for debugging
      protected:
        String customHeader = "";
        char method[7]{'G','E','T'}; // "DELETE" is longest method
        char hostName[64];
        int hostPort = 80;
        char endPoint[64]; // TODO: confirm this is a reasonable length for general use
        String body = "";
    };
  }
}
