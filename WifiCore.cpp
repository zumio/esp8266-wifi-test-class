// Some methods modified/adapted from WeeESP8266.  I owe a great debt of gratitude to the author of
// this library, even though in the end it didn't work correctly for me.  It was a bit challenging
// to debug/find where things were going wrong.
// 
// Ultimately the WeeESP8266 library suffers the same problem with memory etc. that I do with this 
// hand-built library.


#include "Arduino.h"
#include <SoftwareSerial.h>
#include "WifiCore.h"

using namespace Lormo::Api;

/* ################################################################# */

// WifiBase is an abstract base class supporting all the classes with basic functionality related
// to AT command processing
WifiBase::WifiBase() {
}


// Constructor.  Must be called by all child classes to establish working serial connection.
void WifiBase::begin() {
  //SoftwareSerial serial = SoftwareSerial(RX_PIN, TX_PIN);
  _espSerial.begin(9600);

  // setup the hardware reset pin.  Should be HIGH in operation, dropping to LOW (ground) to reset.
  pinMode(RST_PIN, OUTPUT);
  digitalWrite(RST_PIN, HIGH);
}


// Constructor overload enabling the setting of the serial.
// I've since refactored to initialise the SoftwareSerial at member declaration (in the header file)
// so am not sure if this works correctly.
void WifiBase::begin(SoftwareSerial& serial) {
  _espSerial = serial;
  _espSerial.begin(9600);
  
  // setup the hardware reset pin.  Should be HIGH in operation, dropping to LOW (ground) to reset.
  pinMode(RST_PIN, OUTPUT);
  digitalWrite(RST_PIN, HIGH);
}


// Uses strstr on the underlying C strings to determine if the target is in the search string.
// This is used because I was getting incorrect results from the String.indexOf() method.
bool WifiBase::containsString(String searchIn, String searchFor) {
  return (strstr(searchIn.c_str(), searchFor.c_str()) != NULL);
}


// Reads the serial buffer until it is cleared.
// This is needed as sometimes the ESP8266 outputs cruft into the serial buffer.
// Also because some read methods bail before the full buffer is read.
void WifiBase::emptySerialBuffer() 
{
  while(_espSerial.available() > 0) {
    _espSerial.read();
  }
}


// Sends the AT command (C string) to the software serial.  Note: this method emptys the serial
// buffer before sending.  Use _espSerial.println() if this is not the desired behaviour.
void WifiBase::sendAT(char cmd[32]) {
  emptySerialBuffer();
  _espSerial.println(cmd);
}


// Sends the AT command (Arduino String) to the software serial.  Note: this method emptys the serial
// buffer before sending.  Use _espSerial.println() if this is not the desired behaviour.
void WifiBase::sendAT(String cmd) {
  emptySerialBuffer();
  _espSerial.println(cmd);
}


// Outputs the contents of the ESP serial buffer to hardware Serial, primarily for debugging.
void WifiBase::printResponse()
{
  char a;
  unsigned long start = millis();
  while (millis() - start < 5000) { // hard-code timeout to 5 secs
      while(_espSerial.available() > 0) {
          a = _espSerial.read();
          if(a == '\0') continue;
          Serial.print(a);
      }
  }
}


// Returns the serial response to an AT command as a string.  Will exit if target string is located.  
// Adapted from WeeESP8266 library.  Skips null characters that are returned.  NOTE: this will wait 
// the full length of the timeout if the string isn't found.
// My hypothesis is that the timeouts are used as the _espSerial.available() sometimes returns 0
// before the complete string is returned.
String WifiBase::getATResponseUpToString(String target, uint32_t timeout=AT_TIMEOUT)
{
    String data;
    char a;
    unsigned long start = millis();
    while (millis() - start < timeout) {
        while(_espSerial.available() > 0) {
            a = _espSerial.read();
            if (a == '\0') continue;
            data += a;
        }
        // if we find the string we're expecting, return the data without waiting for the 
        // timeout to expire
        if (containsString(data, target)) break;
    }
    return data;
}


// Same as getATResponseUpToString except there is no searching for strings (i.e. it 
// returns the entire string).  NOTE: this waits the entire timeout period before returning.
// This is an adaptation of code from the WeeESP8266 library.
// My hypothesis is that the timeouts are used as the _espSerial.available() sometimes returns 0
// before the complete string is returned.
String WifiBase::getATResponse(uint32_t timeout=AT_TIMEOUT)
{
  String data;
  char a;
  unsigned long start = millis();
  while (millis() - start < timeout) {
    while(_espSerial.available() > 0) {
      a = _espSerial.read();
      if (a == '\0') continue;
      data += a;
    }
  }
  return data;
}


// Watches the response up to the point that the target string occurs.  Returns true if string found, 
// false if not.
bool WifiBase::ATResponseContainsString(String target, uint32_t timeout=AT_TIMEOUT)
{
    String resp = getATResponseUpToString(target, timeout);
    return containsString(resp, target);
}


// Returns the substring from the AT response between the begin + end phrase.  The value is passed 
// by reference via the "data" argument.
bool WifiBase::getATResponseSubstring(String begin, String end, String &data, uint32_t timeout=AT_TIMEOUT)
{
    String resp = getATResponse(timeout);
    data = getSubstring(resp, begin, end);
}


// As per getATResponseSubstring(), but on the passed content argument.
String WifiBase::getSubstring(const String &content, const String &begin, const String &end)
{
  // note that I've had issues with String.indexOf() in some instances
  // but none that have affected this method, so I'm still using here
  // might be better to use strstr, as per containsString(), to avoid problems
  int offset = begin.length();
  int32_t index1 = content.indexOf(begin);
  int32_t index2 = content.indexOf(end, index1+offset);
  String rtn = "";
  if (index1 != -1 && index2 != -1) {
      index1 += begin.length();
      rtn = content.substring(index1, index2);
  }
  return rtn;
}


// Drops the reset pin to ground, then back to high, theoretically causing a hardware
// reset.  This doesn't appear to work in my early tests.
void WifiBase::hardReset() {
  digitalWrite(RST_PIN, LOW);
  delay(500);
  digitalWrite(RST_PIN, HIGH);
}







/* ################################################################# */

// WifiConnection handles basic connection functions using the ESP8266 chip
WifiConnection::WifiConnection() {
}

// Constructor.
void WifiConnection::begin() {
  WifiBase::begin();
}


// Constructor overload with passed SoftwareSerial instance.
void WifiConnection::begin(SoftwareSerial& serial) {
  WifiBase::begin(serial);
}


// Attempts connection to the specified network (ssid) using the provided password (psk).
// Note that this method will first check if already connected.  Use "disconnect()" prior
// if you wish to force a fresh connection.
// Note this method doesn't support retries etc. and has limited error responses (mostly
// just prints the error to the Serial.
bool WifiConnection::connect(char ssid[64], char psk[64]) {
  memcpy(_ssid, ssid, strlen(ssid)+1);
  memcpy(_psk, psk, strlen(psk)+1);

  Serial.print("Connecting to ");
  Serial.print(_ssid);

  // Give the ESP8266 a kick and make sure we get a response
  Serial.print(".");
  char cmd[] = "AT";
  sendAT(cmd);
  // returns:
//AT
//
//
//
//OK
  if (!ATResponseContainsString("OK")) {
    hardReset();
    Serial.println("Error communicating with ESP826");
    return false;
  }

  // Switch to STATION mode.
  Serial.print(".");
  sendAT("AT+CWMODE=1");
  if (!ATResponseContainsString("OK")) {
    Serial.println("Error switching to station mode");
    return false;
  }

  // Check if we're already connected to the target network.
  Serial.print(".");
  sendAT("AT+CWJAP?");
  // returns:
//AT+CWJAP?
//
//+CWJAP:"<ssid>"
//
//OK
// or:
//AT+CWJAP?
// No AP
//
//OK
  Serial.print(".");
  String d = getATResponse();
  if (containsString(d, "+CWJAP:\"")) {
    String currAP = getSubstring(d, "+CWJAP:\"", "\"");
    if (currAP != "") {
      Serial.print("from ");
      Serial.print(currAP);
    }

    // The current AP value is the same as the target SSID
    if (strcmp(currAP.c_str(), _ssid) == 0) {
      // already connected
      Serial.print(" (already connected)");
    }
  } else { 
    if (!containsString(d, "No AP")) {
      Serial.print("Not connected.");
    } else {
      // a different AP is selected
      Serial.print("Switching AP.");
    }

    // in either case, attempt to connect
    // AT+CWJAP="<ssid>","<psk>"
    char cmd[64];
    snprintf(cmd, sizeof(cmd), "AT+CWJAP=\"%s\",\"%s\"", _ssid, _psk);
    //Serial.println(cmd);
    sendAT(cmd);
    // this command seems to take a little while longer than most, allow 10 seconds timeout
    if (ATResponseContainsString("OK", 10000)) {
      Serial.print("OK");
    } else {
      Serial.print("ERROR");
      return false;
    }
  }


  // Switch from multiple connections to a single connection (for simplicity).
  Serial.print(".");
  sendAT("AT+CIPMUX=0");
  if (!ATResponseContainsString("OK")) {
    Serial.print("MUX ERROR");
    return false; // as command syntax is dependent on this, need to fail
  }

  Serial.println(".");
  
  return true;
}


// Disconnects from the wifi network ("AT+CWQAP")
bool WifiConnection::disconnect() {
  sendAT("AT+CWQAP");
  if (!ATResponseContainsString("OK")) {
    Serial.println("Error disconnecting from AP");
    return false;
  }
  return true;
}






/* ################################################################# */

// WifiHTTPResponse handles HTTP-related responses using wifi
WifiHTTPResponse::WifiHTTPResponse() {
}


// Constructor.
void WifiHTTPResponse::begin() {
  WifiBase::begin();
}


// Constructor overload with passed SoftwareSerial instance.
void WifiHTTPResponse::begin(SoftwareSerial& serial) {
  WifiBase::begin(serial);
}


// Returns true if the full response has been received/an HTTP status code can be determined.
// NOT YET IMPLEMENTED (always returns true).
bool WifiHTTPResponse::isReady() {
  return true;
}


// Receives an HTTP response (i.e. response from "AT+CIPSEND" call).
// Note that such responses can occur in dribs and drabs--i.e. the _espSerial.available() may = 0 before
// the full response has been received.
// Adapted from WeeESP8266 library.  Handles multiple connections, but I haven't tried to get those
// to work (only working in single mode).
// Data takes the form of
/* +IPD,<id>,<len>:<data> */
/* +IPD,<len>:<data> */
// depending on whether MUX is enabled or not
// There seems to be additional information that is appended at the end (+IPD,5:0 etc.) also.
// Lastly, printing the output to Serial indicates there is some stuff before, including cruft
// that looks like data returned using a bad baud rate (garbage).
// As I'm running into memory issues trying to store even just a small component of the response
// to check HTTP status code, I've been unable to implement anything that parses the returned data
// and avoids relying on the timeout value to determine "end of stream".
// @param buffer is how the response string is returned
// @param buffer_size = the size (in bytes) of the buffer
// @param data_len is how the size of the return value is returned
uint32_t WifiHTTPResponse::recvPkg(uint8_t *buffer, uint32_t buffer_size, uint32_t *data_len, uint32_t timeout, uint8_t *coming_mux_id)
{
    String data;
    char a;
    int32_t index_PIPDcomma = -1;
    int32_t index_colon = -1; /* : */
    int32_t index_comma = -1; /* , */
    int32_t len = -1;
    int8_t id = -1;
    bool has_data = false;
    uint32_t ret;
    unsigned long start;
    uint32_t i;
    
    if (buffer == NULL) {
        return 0;
    }
    
    start = millis();
    while (millis() - start < timeout) {
        if(_espSerial.available() > 0) {
            a = _espSerial.read();
            data += a;
        }
        
        index_PIPDcomma = data.indexOf("+IPD,");
        if (index_PIPDcomma != -1) {
            index_colon = data.indexOf(':', index_PIPDcomma + 5);
            if (index_colon != -1) {
                index_comma = data.indexOf(',', index_PIPDcomma + 5);
                /* +IPD,id,len:data */
                if (index_comma != -1 && index_comma < index_colon) { 
                    id = data.substring(index_PIPDcomma + 5, index_comma).toInt();
                    if (id < 0 || id > 4) {
                        return 0;
                    }
                    len = data.substring(index_comma + 1, index_colon).toInt();
                    if (len <= 0) {
                        return 0;
                    }
                } else { /* +IPD,len:data */
                    len = data.substring(index_PIPDcomma + 5, index_colon).toInt();
                    if (len <= 0) {
                        return 0;
                    }
                }
                has_data = true;
                break;
            }
        }
    }
    
    if (has_data) {
        i = 0;
        ret = len > buffer_size ? buffer_size : len;
        start = millis();
        while (millis() - start < 3000) {
            while(_espSerial.available() > 0 && i < ret) {
                a = _espSerial.read();
                buffer[i++] = a;
            }
            if (i == ret) {
                emptySerialBuffer();
                if (data_len) {
                    *data_len = len;    
                }
                if (index_comma != -1 && coming_mux_id) {
                    *coming_mux_id = id;
                }
                return ret;
            }
        }
    }
    return 0;
}


// Pulls data from the serial port and constructs a C string containing the response
// to a limited buffer length, to enable parsing of the HTTP status code returned
// NOT YET TESTED
int WifiHTTPResponse::handleResponse()
{
  Serial.print("Receiving... ");
  
  // get the response so that we can parse out the HTTP status code
  // I've given up trying to get the full response due to memory constraints
  // NOTE: I originally was using the WeeESP8266 recvPkg() method for this
  //       but reverted to this simplified version when I ran into issues
  // it also, by necessity, requires the full timeout period to execute (which is far from ideal)
  unsigned long start = millis();
  // attempting to set the index value of a char array causes the ESP8266 to become non-responsive,
  // even in other parts of the code.  So I've resorted to using String, but it has it's own issues
//  char responseToStatus[256]{0};
  String responseToStatus = "";
  // NOTE: this approach fails when run outside of the class.  The String building only goes up to
  //       about 180 chars before truncating (in that context) suggesting an out of memory issue
  unsigned int responseIndex = 0;
  char a;
  while (millis() - start < HTTP_TIMEOUT) {
    if(_espSerial.available()) {
      a = (char) _espSerial.read();
      if (responseIndex < 255) {
        responseToStatus += a;
        responseIndex++;
        if (responseIndex == 255) break;
      }
    }
  }
  Serial.print(responseToStatus);
  Serial.print("\n");

  // Theoretically we should at least be able to determine 200 OK
  if (containsString(responseToStatus, "200 OK\r\n")) {
    Serial.println("OK");
  } else {
    Serial.println("HTTP error");
    // don't return here, as we still need to close the connection
  }

  
  // close the connection
  emptySerialBuffer();
  // the following doesn't work
  //_espSerial.println("AT+CIPCLOSE");
  // following this thread https://github.com/adafruit/Adafruit_ESP8266/issues/2
  // suggests following two techniques, the second of which appears to be working
  //_espSerial.println("AT+CIPSERVER=0");
  _espSerial.println("AT+CIPSTATUS"); // need to call this twice to avoid getting errors on the third line
  _espSerial.println("AT+CIPSTATUS");
  _espSerial.println("AT+CIPCLOSE"); // closes the TCP connection
}





/* ################################################################# */

// WifiHTTPRequest class handles functionality for doing basic HTTP requests using the wifi
WifiHTTPRequest::WifiHTTPRequest() {
}


// Constructor overload that provides an "empty" instance (use accessor methods to set values)
void WifiHTTPRequest::begin() {
  WifiBase::begin();
}


// Constructor overload to enable quick provision of all required data to create a valid
// HTTP request string
void WifiHTTPRequest::begin(char hname[64], char path[64], char meth[9], int hport) {
  WifiBase::begin();
  
  setHostName(hname);
  setHostPort(hport);
  setEndPoint(path);
  setMethod(meth);
}


// Returns true if all of the required information has been provided to create a valid/well-formed
// HTTP request.
// Not yet implemented (currently always returns true)
// TODO: validate all required attributes are set
bool WifiHTTPRequest::isReady() {
  return true;
}


// Adds a line in the header of the HTTP request.  Adds a new line char at the end.
// TODO: ideally this would be an associative array or similar implementation
void WifiHTTPRequest::addHeaderLine(String headerLabel, String headerValue) {
  customHeader += headerLabel;
  customHeader += ": ";
  customHeader += headerValue;
  customHeader += "\n";
}


// Accessor for the hostName attribute
bool WifiHTTPRequest::setHostName(char hname[64]) {
  memcpy(hostName, hname, strlen(hname)+1);
  return true;
}


// Accessor for the hostPort attribute
bool WifiHTTPRequest::setHostPort(int hport) {
  hostPort = hport;
  return true;
}


// Accessor for the endPoint attribute
bool WifiHTTPRequest::setEndPoint(char path[64]) {
  memcpy(endPoint, path, strlen(path)+1);
  return true;
}


// Accessor for the method attribute
bool WifiHTTPRequest::setMethod(char meth[9]) {
  // TODO: validate supplied method = GET, POST, PUT, DELETE, or HEAD
  memcpy(method, meth, strlen(meth)+1);
  return true;
}


// Accessor for the body attribute
bool WifiHTTPRequest::setBody(String sbody) {
  body = sbody;
  return true;
}


// Constructs a HTTP request string from the details (e.g. hostName etc.) setup in the object instance.
// Renders to the char array buffer.  In the current implementation "bufferSize" is not required, but
// the original implementation used snprintf, for which this is required, as we can't easily
// calculate the size of a pointer to a char array.
bool WifiHTTPRequest::render(char *buffer, int bufferSize) {
  // this was my original attempt at creating a human-readable method of constructing a POST 
  // request.  I know this doesn't work for GET and potentially other request types--this was
  // just my initial "bare minimum" attempt
//  char requestTemplate[152] = "%s %s HTTP/1.1\n"
//    "Host: %s\n"
//    "Connection: keep-alive\n" // had issues with this being set to "close"
//    "%s"
//    "Content-Type: application/x-www-form-urlencoded\n"
//    "Content-Length: %i"
//    "\n\n%s";
//    
//  snprintf(buffer, bufferSize, requestTemplate, method, endPoint, hostName, customHeader.c_str(), body.length(), body.c_str());

  // thinking that the extra 152 chars for the template might be causing some of the memory issues
  // I then setup this alternate approach.  Same result, but only buffer is needed in memory
  // I'm sure there must be better ways to do this...
  strcpy(buffer, method);
  strcat(buffer, " ");
  strcat(buffer, endPoint);
  strcat(buffer, " HTTP/1.1\nHost:");
  strcat(buffer, hostName);
  strcat(buffer, "\nConnection: keep-alive\n");
  strcat(buffer, customHeader.c_str());
  strcat(buffer, "Content-Type: application/x-www-form-urlencoded\nContent-Length: ");
  char len[10];
  sprintf(len, "%d", body.length()); // convert to char array
  strcat(buffer, len);
  strcat(buffer, "\n\n");
  strcat(buffer, body.c_str());
  
  return true;
}


// Renders the request to a C string and then sends using AT+CIPSEND
// this could move to the WifiConnection class, with the WifiHTTPRequest object passed by reference
// this may make more sense architecturally
int WifiHTTPRequest::send() {
  Serial.println("WifiHTTPRequest::send()"); // debug statement
  // if we don't have all the data we need to create a well-formed/valid request,
  // don't continue
  if (!isReady()) {
    return -1;
  }
  
  // check the chip is responding
  sendAT("AT");
  if (!ATResponseContainsString("OK")) {
    Serial.println("Error communicating with ESP826");
    return -1; // TODO: setup error codes
  }

  // Attempt a TCP connection to the target server
  char cmd[64]; // TODO: confirm this is sufficient to cover reasonable length host names
  snprintf(cmd, sizeof(cmd), "AT+CIPSTART=\"TCP\",\"%s\",%i", hostName, hostPort);
  // AT+CIPSTART="TCP","api.thingspeak.com",80
  //Serial.println(cmd);
  sendAT(cmd);
  String r = getATResponse();

  // NOTE: this isn't currently working—e.g. when ALREAY is in string, it's outputting "Unexpected response"
  // The current version of the firmware responds with this typo if the connection is already alive
  if (containsString(r, "OK") || containsString(r, "ALREAY")) { // full string "ALREAY CONNECTED"
    // if we get this response, we're ok to continue
    // do nothing
  } else if (containsString(r, "ERROR")) {
    Serial.print("Error connecting to ");
    Serial.print(hostName);
    Serial.print(":");
    Serial.print(hostPort);
    Serial.println("Response:");
    Serial.println(r);
    
    return -1; // TODO: setup error codes
  } else {
    // unknown/unhandled response
    Serial.println("Unexpected response:");
    Serial.println(r);
  }

  Serial.println("Rendering request...");
  
  // either OK or ALREADY CONNECTED, so good to continue
  // send the request
  // the CIPSEND command occurs in two parts
  // the first sends the length
  // a prompt is returned
  // then the HTTP request is sent

  // create a buffer for the request. calcs for my specific use case suggest this will come 
  // in at slightly less than 256 chars, so setting the buffer size accordingly
  char reqString[256]{0};
  render(reqString, 256); // fills the buffer (passed by reference) with the HTTP request
  // note that the need to calc length means that we need to bring this into RAM
  // so we can call strlen on the result
  // the payload may be of slightly different sizes (i.e. a couple of characters difference)
  int reqLength = static_cast<int>(strlen(reqString));
  
  //AT+CIPSEND=<length>
  snprintf(cmd, sizeof(cmd), "AT+CIPSEND=%i", reqLength);
  //Serial.println(cmd); // debug statement
  emptySerialBuffer();
  _espSerial.println(cmd); // we need to wait for the interactive prompt, so can't just use sendAT()
  if (ATResponseContainsString(">")) {
    //Serial.print(request); // debug statement
    // send the HTTP request contents
    emptySerialBuffer(); // just in case there's any cruft in the buffer
    _espSerial.print(reqString);
  } else {
    if (containsString(r, "no ip")) {
      Serial.println("RECONNECT"); // TODO: setup so reconnection can happen
      //connect(_ssid, _psk);
    } else {
      Serial.println("Error sending request.  Response:");
      Serial.print(r);
      Serial.print("\n");
    }
  }
  
  return 1;
}



